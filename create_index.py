#!/usr/bin/python
# coding: utf-8


import datetime
import glob
import hashlib
import string


# Template for index.html file
# project: gitlab project name
# pages_url: URL to base of pages, e.g., group.gitlab-pages.esrf.fr
# wheels_list: html snippet for the list of wheels
# date: date of the creation
index_template = string.Template("""<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>silx development snapshots</title>
</head>
<body>
<h1>silx development snapshots</h1>

<h2>Wheels and source</h2>
<p>To install a package, run:<br>
<code>pip install silx --pre --find-links https://$pages_url/$project/</code>
</p>
<h3>Available files:</h3>
<!-- List of .whl and .tar.gz --!>
<p>
$wheels_list
</p>

<h2>Debian packages</h2>
<p>
It provides Debian 8 (Jessie) and Debian 9 (Stretch) packages for amd64 computers.
</p>
<p>
Packages are built automatically, hence not signed.
</p>

<p>
To use this Debian repository (requires root access):
<ul>
    <li>Add a <code>/etc/apt/source.list.d/$project.list</code> file containing either:<br>
        <code>deb https://$pages_url/$project/ jessie main</code><br>
        or:<br>
        <code>deb https://$pages_url/$project/ stretch main</code>
 </li>
 <li>Run: <code>apt-get update</code></li>
</ul>
</p>

<h3>Available packages</h3>
<ul>
<li><a href="./buster.txt">Buster</a></li>
<li><a href="./stretch.txt">Stretch</a></li>
<li><a href="./jessie.txt">Jessie</a></li>
</ul>

<h3>Troubleshooting</h3>
<p>
<ul>
 <li>To change the priority of this repository, add a <code>/etc/apt/preferences.d/$project.list</code> file containing:<br>
<pre><code>Package: *
Pin: origin "$pages_url/$project/"
Pin-Priority: 600</code></pre>
 </li>
</ul>
</p>

<p><small>Updated: $date</small></p>
</body>
</html>
""")


# template for a link to one wheel/tarball
wheel_template = string.Template("""
<li><a href="./$filename">$filename</a> - SHA256: <small><code>$sha256</code></small></li>\n
""")

# Create list of 
wheels_list = []
for filename in sorted(glob.glob("*.whl") + glob.glob("*.tar.gz")):
    hasher = hashlib.sha256()
    with open(filename, 'rb') as f:
        hasher.update(f.read())

    wheels_list.append(wheel_template.substitute(
        filename=filename,
        sha256=hasher.hexdigest()))

wheels_list = "<ul>\n" + "".join(wheels_list) + "</ul>\n"

index_content = index_template.substitute(
    project="bob",
    pages_url="tvincent.gitlab-pages.esrf.fr",
    wheels_list=wheels_list,
    date=datetime.datetime.now().strftime('%d-%m-%Y %H:%M'))
print(index_content)
