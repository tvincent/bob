# silx development builds
[![pipeline status](https://gitlab.esrf.fr/tvincent/bob/badges/master/pipeline.svg)](https://gitlab.esrf.fr/tvincent/bob/commits/master)

Project building debian packages, Windows and manylinux wheels for [silx](https://github.com/silx-kit/silx).

Access the latest [development packages](https://tvincent.gitlab-pages.esrf.fr/bob/).
