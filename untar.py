#!/usr/bin/env python
# coding: utf-8
"""Untar file(s) given as first argument (template accepted) in current directory"""


import glob
import os
import sys
import tarfile

print('File pattern:', sys.argv[1])
for filename in glob.glob(sys.argv[1]):
    print('Untar:', filename)
    handle = tarfile.open(name=filename, mode='r')
    handle.extractall(path='.')
